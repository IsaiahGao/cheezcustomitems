package customs.interfaces;

import org.bukkit.event.player.PlayerInteractEvent;

public interface LeftClickable {
	
	void handleLeftClick(PlayerInteractEvent e);

}
