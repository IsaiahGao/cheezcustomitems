package customs.interfaces;

import org.bukkit.event.player.PlayerInteractEntityEvent;

public interface InteractEntityable {
	
	void handle(PlayerInteractEntityEvent e);

}
