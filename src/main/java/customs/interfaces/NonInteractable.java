package customs.interfaces;

import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public interface NonInteractable extends InteractEntityable, LeftClickable, RightClickable {

	@Override
	default void handleRightClick(PlayerInteractEvent e) {
		e.setCancelled(true);
	}

	@Override
	default void handleLeftClick(PlayerInteractEvent e) {
		e.setCancelled(true);
	}

	@Override
	default void handle(PlayerInteractEntityEvent e) {
		e.setCancelled(true);
	}

}
