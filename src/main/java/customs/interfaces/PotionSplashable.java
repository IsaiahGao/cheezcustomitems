package customs.interfaces;

import org.bukkit.event.entity.PotionSplashEvent;

public interface PotionSplashable {

	void handle(PotionSplashEvent e);
}
