package customs.interfaces;

import org.bukkit.event.entity.EntityDropItemEvent;

public interface Droppable {
	
	void handle(EntityDropItemEvent e);

}
