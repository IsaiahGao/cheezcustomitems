package customs.interfaces;

import org.bukkit.event.entity.EntityPickupItemEvent;

public interface Pickupable {
	
	void handle(EntityPickupItemEvent e);

}
