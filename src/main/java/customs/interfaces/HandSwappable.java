package customs.interfaces;

import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.EquipmentSlot;

public interface HandSwappable {
	
	void handle(PlayerSwapHandItemsEvent e, EquipmentSlot hand);

}
