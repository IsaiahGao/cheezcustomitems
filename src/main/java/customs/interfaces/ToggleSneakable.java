package customs.interfaces;

import org.bukkit.event.player.PlayerToggleSneakEvent;

public interface ToggleSneakable {
	
	void handle(PlayerToggleSneakEvent e);

}
