package customs.interfaces;

import org.bukkit.event.player.PlayerItemConsumeEvent;

public interface Consumable {
	
	void handle(PlayerItemConsumeEvent e);

}
