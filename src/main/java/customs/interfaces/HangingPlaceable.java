package customs.interfaces;

import org.bukkit.event.hanging.HangingPlaceEvent;

public interface HangingPlaceable {
	
	void handle(HangingPlaceEvent e);

}
