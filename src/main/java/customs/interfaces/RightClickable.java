package customs.interfaces;

import org.bukkit.event.player.PlayerInteractEvent;

public interface RightClickable {
	
	void handleRightClick(PlayerInteractEvent e);

}
