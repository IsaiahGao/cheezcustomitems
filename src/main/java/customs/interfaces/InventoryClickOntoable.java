package customs.interfaces;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface InventoryClickOntoable {

	void handleClickOnto(InventoryClickEvent e);
	
}
