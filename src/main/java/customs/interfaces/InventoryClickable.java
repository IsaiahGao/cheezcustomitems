package customs.interfaces;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface InventoryClickable {

	void handle(InventoryClickEvent e);
	
}
