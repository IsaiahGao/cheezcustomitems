package customs;

import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;

import customs.CustomItemRechargeable.RechargeableItem;
import net.minecraft.server.v1_15_R1.*;
import utilities.Utils;

public class RecipeRecharge extends ShapelessRecipes {
    
    public RecipeRecharge(String name, CustomItemRechargeable cir) {
        super(new MinecraftKey("custom", name), "custom",
        		new ItemStack(CraftItemStack.asNMSCopy(cir.getItem()).getItem()),
        		NonNullList.a(RecipeItemStack.a, RecipeItemStack.a(CraftItemStack.asNMSCopy(cir.getItem()).getItem())));
        this.cir = cir;
        this.rechargeItems = this.cir.getRechargeItems();
    }
    
    private CustomItemRechargeable cir;
    private RechargeableItem[] rechargeItems;
    
    private int getRechargeAmount(ItemStack item) {
    	org.bukkit.inventory.ItemStack bukkit = item.getBukkitStack();
    	for (RechargeableItem ri : this.rechargeItems) {
    		if (Utils.compareCustomItems(ri.getItem(), bukkit)) {
    			return ri.getRechargeAmount();
    		}
    	}
    	return -1;
    }
    
    @Override
    public boolean a(InventoryCrafting inventorycrafting, World world) {
        int i = 0;
        ItemStack itemstack = ItemStack.a;

        for (int j = 0; j < inventorycrafting.getSize(); ++j) {
            ItemStack itemstack1 = inventorycrafting.getItem(j);

            if (!itemstack1.isEmpty()) {
                org.bukkit.inventory.ItemStack bukkitItem = CraftItemStack.asBukkitCopy(itemstack1);
                if (cir.mequals(bukkitItem)) {
                    if (!itemstack.isEmpty()) {
                        return false;
                    }
                    itemstack = itemstack1;
                } else if (getRechargeAmount(itemstack1) > 0) {
                    i += 1;
                } else {
                    return false;
                }
            }
        }

        return !itemstack.isEmpty() && i > 0;
    }

    @Override
    public ItemStack a(InventoryCrafting inventorycrafting) {
        int i = 0;
        ItemStack itemstack = ItemStack.a;

        int k = 0;
        for (int j = 0; j < inventorycrafting.getSize(); ++j) {
            ItemStack itemstack1 = inventorycrafting.getItem(j);
            
            if (!itemstack1.isEmpty()) {
                org.bukkit.inventory.ItemStack bukkitItem = CraftItemStack.asBukkitCopy(itemstack1);
                if (cir.mequals(bukkitItem)) {
                    if (!itemstack.isEmpty()) {
                        return ItemStack.a;
                    }
                    itemstack = itemstack1;
                    org.bukkit.inventory.ItemStack bc = CraftItemStack.asBukkitCopy(itemstack);
                    if (CustomItemRechargeable.getCharge(bc) >= this.cir.getMaxCharge())
                        return ItemStack.a;
                } else if ((k = getRechargeAmount(itemstack1)) > 0) {
                    i += k;
                } else {
                    return ItemStack.a;
                }
            }
        }

        if (!itemstack.isEmpty() && i > 0) {
            ItemStack itemstack2 = itemstack.cloneItemStack();
            org.bukkit.inventory.ItemStack bukkitStack = CraftItemStack.asBukkitCopy(itemstack2);
            bukkitStack = this.cir.addCharge(bukkitStack, i);
            
            itemstack2 = CraftItemStack.asNMSCopy(bukkitStack);
            return itemstack2;
        }
        return ItemStack.a;
    }
}
