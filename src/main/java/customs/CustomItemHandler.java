package customs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import utilities.Utils;

public class CustomItemHandler implements Listener {
	
	public CustomItemHandler() {
		this.items = new HashMap<>();
		this.customItemDatabase = new HashMap<>();
	}
	
	private Map<Material, List<CustomItem>> items;
	private Map<String, CustomItem> customItemDatabase;
	
	public void register(CustomItem ci) {
		if (ci.getItemId() == null) {
			String name = Utils.getPrettyItemName(ci.getItem());
			System.out.println("[[[ ERROR: " + name + " HAS NO CUSTOM ITEM ID!! ]]]");
			return;
		}
		
		if (ci.getItems() == null) {
			Material mat = ci.getItem().getType();
			List<CustomItem> list = items.get(mat);
			if (list == null) {
				items.put(mat, list = new ArrayList<>());
			}
			
			list.add(ci);
			if (ci.getItemId() != null) {
				if (customItemDatabase.put(ci.getItemId(), ci) != null) {
					String name = Utils.getPrettyItemName(ci.getItem());
					System.out.println("[[[ ERROR: " + ci.getItemId() + " / " + name + " IS A DUPLICATE CUSTOM ITEM ID!! ]]]");
				}
			}
		} else {
			for (ItemStack item : ci.getItems()) {
				Material mat = item.getType();
				List<CustomItem> list = items.get(mat);
				if (list == null) {
					items.put(mat, list = new ArrayList<>());
				}
				
				list.add(ci);
			}
		}
	}
	
	public @Nullable CustomItem get(String name) {
		return customItemDatabase.get(name);
	}
	
	public CustomItem get(ItemStack item) {
		if (item == null) {
			return null;
		}
		
		List<CustomItem> list = this.items.get(item.getType());
		if (list == null) {
			return null; 
		}
		
		for (CustomItem ci : list) {
			if (ci.matches(item))
				return ci;
		}
		return null;
	}
	
	@EventHandler
	public void itemConsume(PlayerItemConsumeEvent e) {
		CustomItem item = this.get(e.getItem());
		if (item == null)
			return;
		
		item.consume(e);
	}
	
	@EventHandler
	public void toggleSneak(PlayerToggleSneakEvent e) {
		CustomItem item = this.get(e.getPlayer().getInventory().getItemInMainHand());
		if (item == null)
			return;
		
		item.toggleSneak(e);
	}
	
	@EventHandler
	public void inventoryClick(InventoryClickEvent e) {
		CustomItem item = this.get(e.getCurrentItem());
		if (item != null)
			item.inventoryClick(e);
		
		CustomItem item2 = this.get(e.getCursor());
		if (item2 != null)
			item2.inventoryClickOnto(e);
		
	}
	
	@EventHandler
	public void playerInteract(PlayerInteractEvent e) {
		CustomItem item = this.get(e.getItem());
		if (item == null)
			return;
		
		item.interact(e);
	}
	
	@EventHandler
	public void potionSplash(PotionSplashEvent e) {
		CustomItem item = this.get(e.getEntity().getItem());
		if (item == null)
			return;
		
		item.potionSplash(e);
	}
	
	@EventHandler
	public void playerInteractEntity(PlayerInteractEntityEvent e) {
		CustomItem item = this.get(e.getHand() == EquipmentSlot.HAND ? e.getPlayer().getInventory().getItemInMainHand() : e.getPlayer().getInventory().getItemInOffHand());
		if (item == null)
			return;
		
		item.interactEntity(e);
	}

	@EventHandler
	public void playerDropItem(EntityDropItemEvent e) {
		CustomItem item = this.get(e.getItemDrop().getItemStack());
		if (item == null)
			return;
		
		item.drop(e);
	}

	@EventHandler
	public void playerPickupItem(EntityPickupItemEvent e) {
		CustomItem item = this.get(e.getItem().getItemStack());
		if (item == null)
			return;
		
		item.pickup(e);
	}
	
	@EventHandler
	public void playerHangingPlace(HangingPlaceEvent e) {
//		CustomItem item = this.get(e.getEntity().);
//		if (item == null)
//			return;
//		
//		item.hangingPlace(e);
	}
	
	@EventHandler
	public void swapHand(PlayerSwapHandItemsEvent e) {
		CustomItem item = this.get(e.getMainHandItem());
		if (item != null) {
			item.handSwap(e, EquipmentSlot.HAND);
		}
		
		CustomItem item2 = this.get(e.getOffHandItem());
		if (item2 != null) {
			item2.handSwap(e, EquipmentSlot.OFF_HAND);
		}
	}

}
