package customs;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.minecraft.server.v1_15_R1.MinecraftServer;
import utilities.YoureAnIdiotException;
import utilities.itembuilder.ItemNBT;

public class CustomItemRechargeable extends CustomItem {
	
	public static class RechargeableItem {
		public RechargeableItem(ItemStack item, int rechargeAmt) {
			this.item = item;
			this.rechargeAmt = rechargeAmt;
		}
		
		private ItemStack item;
		private int rechargeAmt;
		
		public ItemStack getItem() {
			return item;
		}
		
		public int getRechargeAmount() {
			return rechargeAmt;
		}
	}
	
	public static RechargeableItem a(ItemStack rechargeItem, int rechargeAmt) {
		return new RechargeableItem(rechargeItem, rechargeAmt);
	}

	public CustomItemRechargeable(String name, ItemStack item, int initialCharge, int maxCharge, ItemStack rechargeItem, int rechargeAmt) {
		this(name, item, initialCharge, maxCharge, new RechargeableItem(rechargeItem, rechargeAmt));
	}

	public CustomItemRechargeable(String name, ItemStack item, int initialCharge, int maxCharge, RechargeableItem... rechargeables) {
		super(name, item);
		this.maxCharge = maxCharge;
		this.rechargeables = rechargeables;
		
		if (initialCharge > 0 && maxCharge > 0) {
			ItemMeta meta = this.item.getItemMeta();
			List<String> lore = meta.getLore();
			if (lore == null)
				lore = new ArrayList<>();
			lore.add("§eCharge Remaining:");
			lore.add("§f[]");
			meta.setLore(lore);
			this.item.setItemMeta(meta);
			this.item = get(initialCharge);
			
			if (rechargeables.length > 0) {
				RecipeRecharge rech = new RecipeRecharge(name, this);
				MinecraftServer.getServer().getCraftingManager().addRecipe(rech);
			}
		}
	}
	
	private int maxCharge;
	private RechargeableItem[] rechargeables;
	
	public int getMaxCharge() {
		return this.maxCharge;
	}
	
	public RechargeableItem[] getRechargeItems() {
		return this.rechargeables;
	}
	
    public ItemStack get(int charge) {
        ItemStack stack = getItem().clone();
        stack = addCharge(stack, charge);
        return stack;
    }
    
    public ItemStack convert(ItemStack stack, int charge) {
        ItemMeta meta = stack.getItemMeta();
        if (!meta.hasDisplayName()) {
        	meta.setDisplayName(getItem().getItemMeta().getDisplayName());
        }
        
        List<String> lore = meta.getLore();
        if (lore == null) {
            lore = getItem().getLore();
        } else {
            lore.addAll(0, getItem().getLore());
        }
        
        boolean flag = false;
        for (int i = 0; i < lore.size(); i++) {
            if (!lore.get(i).startsWith("§f["))
                continue;
            
            lore.set(i, barFor(charge / (float) maxCharge, 'b'));
            meta.setLore(lore);
            stack.setItemMeta(meta);
            flag = true;
            break;
        }
        
        stack = ItemNBT.setNBTValue(stack, "ItemCharge", charge);
        stack = ItemNBT.setNBTValue(stack, "CustomItemID", this.itemId);
        if (!flag)
            throw new YoureAnIdiotException("setCharge failed - no viable string found!");
        return stack;
    }

    public ItemStack setCharge(ItemStack stack, int charge) {
    	return setCharge(stack, charge, maxCharge);
    }
    
    public static ItemStack setCharge(ItemStack stack, int charge, int maxCharge) {
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = meta.getLore();

        boolean flag = false;
        for (int i = 0; i < lore.size(); i++) {
            if (!lore.get(i).startsWith("§f["))
                continue;
            
            lore.set(i, barFor(charge / (float) maxCharge, 'b'));
            meta.setLore(lore);
            stack.setItemMeta(meta);
            flag = true;
            break;
        }
        
        if (!flag) {
			lore.add("§eCharge Remaining:");
            lore.add(barFor(charge / (float) maxCharge, 'b'));
            meta.setLore(lore);
            stack.setItemMeta(meta);
        }
        
        stack = ItemNBT.setNBTValue(stack, "ItemCharge", charge);
        return stack;
    }
    
    public static int getCharge(ItemStack stack) {
    	int charge = ItemNBT.getNBTValueIntOrDefault(stack, "ItemCharge", 0);
    	if (charge > -1)
    		return charge;
    	
        throw new YoureAnIdiotException("getCharge failed - no matching charge found!");
    }
    
    public ItemStack addCharge(ItemStack stack, int amount) {
        int charge = getCharge(stack);
        if (charge < 0)
            return null;

        charge = charge + amount;
        if (charge < 0)
            charge = 0;
        else if (charge > maxCharge)
            charge = maxCharge;
        
        return setCharge(stack, charge);
    }
    
    public static ItemStack addCharge(ItemStack stack, int amount, int maxCharge) {
        int charge = getCharge(stack);
        if (charge < 0)
            return null;

        charge = charge + amount;
        if (charge < 0)
            charge = 0;
        else if (charge > maxCharge)
            charge = maxCharge;
        
        return setCharge(stack, charge, maxCharge);
    }
    
    public static String barFor(float f, char color) {
        String off = "§8";
        if (f > 1.1) {
            color = 'e';
            off = "§a";
            f -= 1;
        }
        
        StringBuilder builder = new StringBuilder("§f[§" + color);
        int c = (int) Math.ceil(f * 50);
        for (int i = 0; i < 50; i++) {
            if (c-- <= 0) {
                builder.append(off);
            }
            
            builder.append(';');
        }
        builder.append("§f] ");
        return builder.toString();
    }
    
}
