package customs;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import customs.interfaces.InventoryClickable;
import utilities.Utils;
import utilities.itembuilder.ItemNBT;

public abstract class CustomItemDropUsable extends CustomItem implements InventoryClickable {

	public CustomItemDropUsable(ItemStack item) {
		this(ItemNBT.getNBTValueString(item, "CustomItemID"), item);
	}

	public CustomItemDropUsable(String id, ItemStack item) {
		super(id, item);
	}
	
	public abstract boolean use(Player p, ItemStack item);
	public abstract boolean canUse(Player p);
	public abstract boolean consumeItem();

	@Override
	public void handle(InventoryClickEvent e) {
		if (!(e.getWhoClicked() instanceof Player))
			return;
		
		Player p = (Player) e.getWhoClicked();
		if (e.getAction() == InventoryAction.DROP_ONE_SLOT && canUse(p)) {
			e.setCancelled(true);
			
			if (use(p, e.getCurrentItem()) && consumeItem()) {
				ItemStack item = e.getCurrentItem();
				int amt = item.getAmount();
				e.setCurrentItem(amt == 1 ? null : Utils.setItemAmount(item, amt - 1));
			}
		}
	}

}
