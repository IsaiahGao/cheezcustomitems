package customs;

import org.bukkit.inventory.ItemStack;

import customs.interfaces.NonInteractable;
import utilities.itembuilder.ItemNBT;

public class CustomItemNoninteractable extends CustomItem implements NonInteractable {

	public CustomItemNoninteractable(ItemStack item) {
		this(ItemNBT.getNBTValueString(item, "CustomItemID"), item);
	}

	public CustomItemNoninteractable(String id, ItemStack item) {
		super(id, item);
	}

}
