package customs;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private static Main inst;
	private static CustomItemHandler handler;
	
	@Override
	public void onEnable() {
		inst = this;
		if (handler == null)
			handler = new CustomItemHandler();
		this.getServer().getPluginManager().registerEvents(handler, this);
	}
	
	public static Main instance() {
		return inst;
	}
	
	public static CustomItemHandler getItemHandler() {
		if (handler == null)
			handler = new CustomItemHandler();
		return handler;
	}

}
