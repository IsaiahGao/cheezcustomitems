package customs;

import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import customs.interfaces.*;
import utilities.Utils;
import utilities.itembuilder.ItemNBT;

public abstract class CustomItem {
	
	/**
	 * @Deprecated Replaced lore based detection with string based.
	 */
	public CustomItem(ItemStack item) {
		this(ItemNBT.getNBTValueString(item, "CustomItemID"), item);
	}

//	/**
//	 * @Deprecated Replaced lore based detection with string based.
//	 */
//	@Deprecated
//	public CustomItem(ItemStack item, int importantLoreLine) {
//		this.item = item;
//		this.lorelines = importantLoreLine;
//		Main.getItemHandler().register(this);
//	}

//	/**
//	 * @Deprecated Replaced lore based detection with string based.
//	 */
//	@Deprecated
//	public CustomItem(ItemStack[] items, int importantLoreLine) {
//		this.item = items[0];
//		this.items = items;
//		this.lorelines = importantLoreLine;
//		Main.getItemHandler().register(this);
//	}

	public CustomItem(ItemStack[] items) {
		String id = ItemNBT.getNBTValueString(items[0], "CustomItemID");
		this.item = ItemNBT.setNBTValue(items[0], "CustomItemID", id);
		this.items = items;
		for (int i = 0; i < this.items.length; i++) {
			this.items[i] = ItemNBT.setNBTValue(items[i], "CustomItemID", id);
		}
		this.itemId = id;
		Main.getItemHandler().register(this);
		this.assertId();
	}

	public CustomItem(String id, ItemStack item) {
		if (id == null) {
			this.item = item;
			this.lorelines = 0;
			Main.getItemHandler().register(this);
			return;
		}
		this.item = ItemNBT.setNBTValue(item, "CustomItemID", id);
		this.itemId = id;
		Main.getItemHandler().register(this);
		this.assertId();
	}

	public CustomItem(String id, ItemStack[] items) {
		this.item = ItemNBT.setNBTValue(items[0], "CustomItemID", id);
		this.itemId = id;
		Main.getItemHandler().register(this);
		this.assertId();
	}
	
	protected ItemStack[] items;
	protected ItemStack item;
	protected int lorelines;
	protected String itemId;
	
	private void assertId() {
		if (this.itemId == null) {
			System.out.println("[ERROR] " + Utils.getPrettierItemName(this.item) + " has no CustomItemID!!!");
		}
	}
	
	public ItemStack[] getItems() {
		return this.items;
	}
	
	public ItemStack getItem() {
		return this.item;
	}
	
	public boolean matches(ItemStack other) {
		this.assertId();
		return this.itemId.equals(ItemNBT.getNBTValueString(other, "CustomItemID"));
	}
	
	public boolean mequals(ItemStack other) {
		if (this.item.getType() == other.getType()) {
			return matches(other);
		}
		return false;
	}
	
	public void interact(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (this instanceof RightClickable) {
				((RightClickable) this).handleRightClick(e);
			}
		} else if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (this instanceof LeftClickable) {
				((LeftClickable) this).handleLeftClick(e);
			}
		}
	}
	
	public void toggleSneak(PlayerToggleSneakEvent e) {
		if (this instanceof ToggleSneakable) {
			((ToggleSneakable) this).handle(e);
		}
	}
	
	public void drop(EntityDropItemEvent e) {
		if (this instanceof Droppable) {
			((Droppable) this).handle(e);
		}
	}
	
	public void pickup(EntityPickupItemEvent e) {
		if (this instanceof Pickupable) {
			((Pickupable) this).handle(e);
		}
	}
	
	public void interactEntity(PlayerInteractEntityEvent e) {
		if (this instanceof InteractEntityable) {
			((InteractEntityable) this).handle(e);
		}
	}

	public void inventoryClick(InventoryClickEvent e) {
		if (this instanceof InventoryClickable) {
			((InventoryClickable) this).handle(e);
		}
	}
	
	public void inventoryClickOnto(InventoryClickEvent e) {
		if (this instanceof InventoryClickOntoable) {
			((InventoryClickOntoable) this).handleClickOnto(e);
		}
	}
	
	public void consume(PlayerItemConsumeEvent e) {
		if (this instanceof Consumable) {
			((Consumable) this).handle(e);
		}
	}
	
	public void potionSplash(PotionSplashEvent e) {
		if (this instanceof PotionSplashable) {
			((PotionSplashable) this).handle(e);
		}
	}
	
	public void handSwap(PlayerSwapHandItemsEvent e, EquipmentSlot hand) {
		if (this instanceof HandSwappable) {
			((HandSwappable) this).handle(e, hand);
		}
	}
	
	public void hangingPlace(HangingPlaceEvent e) {
		if (this instanceof HangingPlaceable)
			((HangingPlaceable) this).handle(e);
	}

	public String getItemId() {
		return this.itemId;
	}

}
